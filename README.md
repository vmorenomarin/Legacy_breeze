# Theme for KDE Plasma 5

Based on default Breeze Light and MacBreeze-Shadowless


It differs from original:
- It has light semi-transparent panel and notifications (so the blur can be enabled) with black font and icons.
- The panel doesn't have a shadow which creates a flat, coherent look with maximized light windows.
- Line Breeze icons used.


To install this theme, either clone/copy this repository or extract in ~/.local/share/plasma/desktoptheme/ for a local user or globally in /usr/share/plasma/desktoptheme/ .
